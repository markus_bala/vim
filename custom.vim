""""""""""""""""SETTING""""""""""""""
set autoindent
set autowrite
set autoread
set foldenable
set nowrap
set nojoinspaces

set relativenumber
set number
set visualbell
set showmode
set showcmd
set hidden
set ttyfast
set ruler
set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
set laststatus=2
set history=1000
set viewoptions=folds,options,cursor,unix,slash " Better Unix / Windows compatibility
set list
set matchtime=3
set nospell

set splitbelow
set splitright
set shiftround

set title
set linebreak
"set colorcolumn=+1
set virtualedit=onemore              " Allow for cursor befond last character
set modelines=5
set cursorline
set scrolloff=10

set encoding=utf-8
set backspace=indent,eol,start
set fillchars=diff:⣿,vert:│
set listchars=tab:▸\ ,trail:·,extends:❯,precedes:❮
set showbreak=↪

set iskeyword-=.                    " '.' is an end of word designator
set iskeyword-=#                    " '#' is an end of word designator
set iskeyword-=-                    " '-' is an end of word designator

set shell=/bin/bash\ --login

" Set options regarding the undo history and undo tree depth
set undofile
set undoreload=10000
set undolevels=512

" Don't try to highlight lines longer than 800 characters.
set synmaxcol=800

" Time out on key codes but not mappings.
" Basically this makes terminal Vim work sanely.
set notimeout
set ttimeout
set ttimeoutlen=10

" Spacing and tabbing {{{
set expandtab
set smarttab
set softtabstop=4
set tabstop=4
set shiftwidth=4
set wrap
set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
set textwidth=80
" }}}


Plug 'easymotion/vim-easymotion'

Plug 'haya14busa/incsearch.vim'

Plug 'haya14busa/incsearch-easymotion.vim'

Plug 'psf/black', { 'branch': 'stable' }

" Airline {{{
let g:airline_theme = 'dark'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
" }}}


" Leader key remapping
let mapleader = ","
let maplocalleader = "\\"
map <leader><space> :noh<cr>

" Convenience mappings
" Stop it, hash key.
inoremap # X<BS>#

inoremap jk <ESC>
nnoremap ; :

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
"
" tab navigation mappings
map tn :tabn<CR>
map tp :tabp<CR>
map tm :tabm 
map tt :tabnew 
map ts :tab split<CR>
map <C-S-Right> :tabn<CR>
imap <C-S-Right> <ESC>:tabn<CR>
map <C-S-Left> :tabp<CR>
imap <C-S-Left> <ESC>:tabp<CR>

" Show contents of registers
nnoremap <leader>r :registers<cr>

" Easier linewise reselection of whatever was just pasted
nnoremap <leader>V V`]

" Indent/dedent/autoindent what you just pasted.
nnoremap <lt>> V`]<
nnoremap ><lt> V`]>
nnoremap =- V`]=

" Keep the cursor in place while joining lines
nnoremap J mzJ`z

" Split line (sister to [J]oin lines)
" The normal use of S is covered by cc, so don't worry about shadowing it.
nnoremap S i<cr><esc>^mwgk:silent! s/\v +$//<cr>:noh<cr>`w

" HTML tag closing
inoremap <C-_> <Space><BS><Esc>:call InsertCloseTag()<cr>a

" Select (charwise) the contents of the current line, excluding indentation.
" Great for pasting Python lines into REPLs.
nnoremap vv ^vg_

" Sudo to write
cnoremap w!! w !sudo tee % >/dev/null
"
" Fix linewise visual selection of various text objects
nnoremap VV V
nnoremap Vit vitVkoj
nnoremap Vat vatV
nnoremap Vab vabV
nnoremap VaB vaBV

" List navigation
" nnoremap <left>  :cprev<cr>zvzz
" nnoremap <right> :cnext<cr>zvzz
" nnoremap <up>    :lprev<cr>zvzz
" nnoremap <down>  :lnext<cr>zvzz

nnoremap <left>  <nop>
nnoremap <right> <nop>
nnoremap <up>    <nop>
nnoremap <down>  <nop>

inoremap <left>  <nop>
inoremap <right> <nop>
inoremap <up>    <nop>
inoremap <down>  <nop>

" set pastetoggle
set pastetoggle=<F8>

nnoremap <Leader>d :vertical resize 
nnoremap <Leader>w :w<CR>

" Jumpt to end of text after paste
vnoremap <silent> y y']
vnoremap <silent> p p']
nnoremap <silent> p p']

"Stop stupid window from popping up
map q: :q

" Spacebar to toggle folds the cursor is over
nnoremap <Space> za
vnoremap <Space> za

" Mappings to access buffers (don't use "\p" because a
" " delay before pressing "p" would accidentally paste).
" " \l       : list buffers
" " \b \f \g : go back/forward/last-used
" " \1 \2 \3 : go to buffer 1/2/3 etc
nnoremap <Leader>l :ls<CR>
" nnoremap <Leader>b :bp<CR>
" nnoremap <Leader>f :bn<CR>
nnoremap <Leader>z :bp<CR>
nnoremap <Leader>m :bn<CR>
nnoremap <Leader>g :e#<CR>
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>
nnoremap <Space><Space>  :ls<CR>:b 



" Typos
command! -bang E e<bang>
command! -bang Q q<bang>
command! -bang W w<bang>
command! -bang QA qa<bang>
command! -bang Qa qa<bang>
command! -bang Wa wa<bang>
command! -bang WA wa<bang>
command! -bang Wq wq<bang>
command! -bang WQ wq<bang>

" Toggle [i]nvisible characters
nnoremap <leader>i :set list!<cr>

" Unfuck my screen
nnoremap U :syntax sync fromstart<cr>:redraw!<cr>

" Searching & Replacing
set ignorecase
set smartcase
set incsearch
set showmatch
set virtualedit+=block

noremap <silent> <leader><space> :noh<cr>:call clearmatches()<cr>

runtime macros/matchit.vim
map <tab> %

" Easier to type, and I never use the default behavior.
noremap H ^
noremap L $
vnoremap L g_

" Some quick shortcuts for yanking and deleting lines.
noremap Y y$
nnoremap D d$

" Fix linewise visual selection of various text objects
nnoremap VV V
nnoremap Vit vitVkoj
nnoremap Vat vatV
nnoremap Vab vabV
nnoremap VaB vaBV

" List navigation
" nnoremap <left>  :cprev<cr>zvzz
" nnoremap <right> :cnext<cr>zvzz
" nnoremap <up>    :lprev<cr>zvzz
" nnoremap <down>  :lnext<cr>zvzz

nnoremap <left>  <nop>
nnoremap <right> <nop>
nnoremap <up>    <nop>
nnoremap <down>  <nop>

inoremap <left>  <nop>
inoremap <right> <nop>
inoremap <up>    <nop>
inoremap <down>  <nop>

" set pastetoggle
set pastetoggle=<F8>

nnoremap <Leader>d :vertical resize 
nnoremap <Leader>w :w<CR>

" Jumpt to end of text after paste
vnoremap <silent> y y']
vnoremap <silent> p p']
nnoremap <silent> p p']

"Stop stupid window from popping up
map q: :q

" Folding Settings
set nofoldenable
set foldlevelstart=0
set foldlevel=0
set foldcolumn=2
set foldmethod=indent
set foldopen-=search
set foldopen-=undo

" Spacebar to toggle folds the cursor is over
nnoremap <Space> za
vnoremap <Space> za

" Set ,z to toggle all folds on/off
nnoremap <leader>f zi
vnoremap <leader>f zi

" Make zO recursively open whatever top level fold we're in, no matter where the
" cursor happens to be.
nnoremap zO zCzO

" "Focus" the current line.  Basically:
"
" 1. Close all folds.
" 2. Open just the folds containing the current line.
" 3. Move the line to a little bit (15 lines) above the center of the screen.
" 4. Pulse the cursor line.  My eyes are bad.
"
" This mapping wipes out the z mark, which I never use.
"
" I use :sus for the rare times I want to actually background Vim.
nnoremap <c-z> mzzMzvzz15<c-e>`z:Pulse<cr>

" Formatting options:
"   t: Auto-wrap text
"   c: Auto-wrap comments
"   r: Auto-insert comment leader after <enter>
"   o: Auto-insert comment leader after o or O
"   q: Allow formatting of comments with 'gq'
"   n: Recognize lists and indent
"   1: If possible, break lines before one-letter words, not after
set formatoptions+=c,r,q,n,1


" EasyMotion {{{
" Move to line
map <Leader><Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader><Leader>L <Plug>(easymotion-overwin-line)

" You can use other keymappings like <C-l> instead of <CR> if you want to
" use these mappings as default search and somtimes want to move cursor with
" EasyMotion.
function! s:incsearch_config(...) abort
    return incsearch#util#deepextend(deepcopy({
                \   'modules': [incsearch#config#easymotion#module({'overwin': 1})],
                \   'keymap': {
                \     "\<CR>": '<Over>(easymotion)'
                \   },
                \   'is_expr': 0
                \ }), get(a:, 1, {}))
endfunction

noremap <silent><expr> <Leader><Leader>/  incsearch#go(<SID>incsearch_config())
noremap <silent><expr> <Leader><Leader>?  incsearch#go(<SID>incsearch_config({'command': '?'}))
noremap <silent><expr> <Leader><Leader>g/ incsearch#go(<SID>incsearch_config({'is_stay': 1}))
" }}}

" incsearch {{{
set hlsearch
let g:incsearch#auto_nohlsearch = 1
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)
" }}}


" Black {{{
" autocmd BufWritePre *.py execute ':Black'
nnoremap <F10> :Black<CR>
" }}}

