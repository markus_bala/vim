1. Run bash setting.sh
2. install below command
--------------------------------
sudo add-apt-repository ppa:jonathonf/vim
sudo apt-get update && sudo apt-get upgrade
sudo apt-get remove vim -y
sudo apt-get install vim -y

sudo apt-get install curl  exuberant-ctags git ack-grep -y
sudo pip install pep8 flake8 pyflakes isort yapf -y

sudo apt-get install build-essential cmake3 -y
sudo apt-get install python-dev python3-dev -y



cd ~/.vim/bundle/YouCompleteMe
YCM_CORES=1 ./install.py --clang-completer

# install java
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
sudo apt-get install oracle-java8-set-default
