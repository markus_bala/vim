set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

""""""""""""""""SETTING""""""""""""""
syntax on
set autoindent
set autowrite
set autoread
set foldenable
set nowrap
set nojoinspaces

set relativenumber
set number
set visualbell
set showmode
set showcmd
set hidden
set ttyfast
set ruler
set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
set laststatus=2
set history=1000
set viewoptions=folds,options,cursor,unix,slash " Better Unix / Windows compatibility
set list
set matchtime=3
set nospell

set splitbelow
set splitright
set shiftround

set title
set linebreak
"set colorcolumn=+1
set virtualedit=onemore              " Allow for cursor befond last character
set modelines=5
set cursorline
set scrolloff=10

set encoding=utf-8
set backspace=indent,eol,start
set fillchars=diff:⣿,vert:│
set listchars=tab:▸\ ,trail:·,extends:❯,precedes:❮
set showbreak=↪

set iskeyword-=.                    " '.' is an end of word designator
set iskeyword-=#                    " '#' is an end of word designator
set iskeyword-=-                    " '-' is an end of word designator

set shell=/bin/bash\ --login

" Set options regarding the undo history and undo tree depth
set undofile
set undoreload=10000
set undolevels=512

" Don't try to highlight lines longer than 800 characters.
set synmaxcol=800

" Time out on key codes but not mappings.
" Basically this makes terminal Vim work sanely.
set notimeout
set ttimeout
set ttimeoutlen=10

" Make Vim able to edit crontab files again.
set backupskip=/tmp/*,/private/tmp/*"

" Better Completion
set complete=.,w,b,u,t
set completeopt=longest,menuone,preview

" Save when losing focus
au FocusLost * :silent! wall

" Resize splits when the window is resized
au VimResized * :wincmd =

" Cline {{{
augroup cline
    au!
    au WinLeave,InsertEnter * set nocursorline
    au WinEnter,InsertLeave * set cursorline
augroup END
" }}}

" Trailing Chars {{{
augroup trailing
    au!
    au InsertEnter * :set listchars+=eol:¬
    au InsertLeave * :set listchars-=eol:¬
augroup END
" }}}

" Wildmode Options {{{
set wildmenu
set wildmode=list:longest,full " Command <Tab> completion, list matches, then longest common part, then all.
set wildignore+=.hg,.git,.svn                    " Version control
set wildignore+=*.aux,*.out,*.toc                " LaTeX intermediate files
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg   " binary images
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest " compiled object files
set wildignore+=*.spl                            " compiled spelling word lists
set wildignore+=*.sw?                            " Vim swap files
set wildignore+=*.DS_Store                       " OSX bullshit
set wildignore+=*.luac                           " Lua byte code
set wildignore+=migrations                       " Django migrations
set wildignore+=*.pyc                            " Python byte code
set wildignore+=*.orig                           " Merge resolution files
" }}}

" Line Return {{{
augroup line_return
    au!
    au BufReadPost *
                \ if line("'\"") > 0 && line("'\"") <= line("$") |
                \     execute 'normal! g`"zvzz' |
                \ endif
augroup END
" }}}

" Spacing and tabbing {{{
set expandtab
set smarttab
set softtabstop=4
set tabstop=4
set shiftwidth=4
set wrap
set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
set textwidth=80
" }}}

" Backup Files {{{
set backup                       " enable backups
set undodir=~/.vim/dirs/undos/     " undo files
set backupdir=~/.vim/dirs/backups/ " backups
set directory=~/.vim/dirs/tmp/   " swap files
let g:yankring_history_dir='~/.vim/dirs/'

" Make those folders automatically if they don't already exist.
if !isdirectory(expand(&undodir))
    call mkdir(expand(&undodir), "p")
endif
if !isdirectory(expand(&backupdir))
    call mkdir(expand(&backupdir), "p")
endif
if !isdirectory(expand(&directory))
    call mkdir(expand(&directory), "p")
endif
" }}}

" Colors {{{
set t_Co=256
let base16colorspace=256
" }}}

" Abbreviations {{{
function! EatChar(pat)
    let c = nr2char(getchar(0))
    return (c =~ a:pat) ? '' : c
endfunction

function! MakeSpacelessIabbrev(from, to)
    execute "iabbrev <silent> ".a:from." ".a:to."<C-R>=EatChar('\\s')<CR>"
endfunction
function! MakeSpacelessBufferIabbrev(from, to)
    execute "iabbrev <silent> <buffer> ".a:from." ".a:to."<C-R>=EatChar('\\s')<CR>"
endfunction

call MakeSpacelessIabbrev('bh/',  'http://hokietux.net/')
iabbrev ldis ಠ_ಠ
iabbrev lsad ಥ_ಥ
iabbrev lhap ಥ‿ಥ
iabbrev lmis ಠ‿ಠ
iabbrev mb@ markusbala@gmail.com
iabbrev vrcf `~/.vimrc` file
" }}}

" Key Re-Mappings {{{
" Leader key remapping
let mapleader = ","
let maplocalleader = "\\"
map <leader><space> :noh<cr>

" Convenience mappings
" Stop it, hash key.
inoremap # X<BS>#

inoremap jk <ESC>

set clipboard+=unnamed

" split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" <ESC> is really far away
"imap <c-d> <ESC>
"nnoremap <c-d> <ESC>
"vnoremap <c-d> <ESC>
"cnoremap <c-d> <ESC>

" Toggle line numbers
" nnoremap <leader>n :setlocal number!<cr>


" Front and center
" Use :sus for the rare times I want to actually background Vim.
nnoremap <c-z> zMzvzz25<c-e>
vnoremap <c-z> <esc>zv`<ztgv

" Sort lines
nnoremap <leader>s vip:!sort<cr>
vnoremap <leader>s :!sort<cr>


" tab navigation mappings
map tn :tabn<CR>
map tp :tabp<CR>
map tm :tabm 
map tt :tabnew 
map ts :tab split<CR>
map <C-S-Right> :tabn<CR>
imap <C-S-Right> <ESC>:tabn<CR>
map <C-S-Left> :tabp<CR>
imap <C-S-Left> <ESC>:tabp<CR>

" Show contents of registers
nnoremap <leader>r :registers<cr>

" Reformat line.
" I never use l as a macro register anyway.
nnoremap ql ^vg_gq

" Easier linewise reselection of whatever was just pasted
nnoremap <leader>V V`]

" Indent/dedent/autoindent what you just pasted.
nnoremap <lt>> V`]<
nnoremap ><lt> V`]>
nnoremap =- V`]=

" Keep the cursor in place while joining lines
nnoremap J mzJ`z

" Split line (sister to [J]oin lines)
" The normal use of S is covered by cc, so don't worry about shadowing it.
nnoremap S i<cr><esc>^mwgk:silent! s/\v +$//<cr>:noh<cr>`w

" HTML tag closing
inoremap <C-_> <Space><BS><Esc>:call InsertCloseTag()<cr>a

" Select (charwise) the contents of the current line, excluding indentation.
" Great for pasting Python lines into REPLs.
nnoremap vv ^vg_

nnoremap ; :

:nnoremap <Tab> :bnext<CR>
:nnoremap <S-Tab> :bprevious<CR>

"Cjoreabbrev W! w!
"cnoreabbrev Q! q!
"cnoreabbrev Qall! qall!
"cnoreabbrev Wq wq
"cnoreabbrev Wa wa
"cnoreabbrev wQ wq
"cnoreabbrev WQ wq
"cnoreabbrev W w
"cnoreabbrev Q q
"cnoreabbrev Qall qall
" }}}

" Sudo to write
cnoremap w!! w !sudo tee % >/dev/null

" Typos
command! -bang E e<bang>
command! -bang Q q<bang>
command! -bang W w<bang>
command! -bang QA qa<bang>
command! -bang Qa qa<bang>
command! -bang Wa wa<bang>
command! -bang WA wa<bang>
command! -bang Wq wq<bang>
command! -bang WQ wq<bang>

" Toggle [i]nvisible characters
nnoremap <leader>i :set list!<cr>

" Unfuck my screen
nnoremap U :syntax sync fromstart<cr>:redraw!<cr>

" Searching & Replacing
set ignorecase
set smartcase
set incsearch
set showmatch
set virtualedit+=block

noremap <silent> <leader><space> :noh<cr>:call clearmatches()<cr>

runtime macros/matchit.vim
map <tab> %

" Easier to type, and I never use the default behavior.
noremap H ^
noremap L $
vnoremap L g_

" Some quick shortcuts for yanking and deleting lines.
noremap Y y$
nnoremap D d$

" Fix linewise visual selection of various text objects
nnoremap VV V
nnoremap Vit vitVkoj
nnoremap Vat vatV
nnoremap Vab vabV
nnoremap VaB vaBV

" List navigation
" nnoremap <left>  :cprev<cr>zvzz
" nnoremap <right> :cnext<cr>zvzz
" nnoremap <up>    :lprev<cr>zvzz
" nnoremap <down>  :lnext<cr>zvzz

nnoremap <left>  <nop>
nnoremap <right> <nop>
nnoremap <up>    <nop>
nnoremap <down>  <nop>

inoremap <left>  <nop>
inoremap <right> <nop>
inoremap <up>    <nop>
inoremap <down>  <nop>

" set pastetoggle
set pastetoggle=<F8>

nnoremap <Leader>d :vertical resize 
nnoremap <Leader>w :w<CR>

" Jumpt to end of text after paste
vnoremap <silent> y y']
vnoremap <silent> p p']
nnoremap <silent> p p']

"Stop stupid window from popping up
map q: :q

" Folding Settings
set nofoldenable
set foldlevelstart=0
set foldlevel=0
set foldcolumn=2
set foldmethod=indent
set foldopen-=search
set foldopen-=undo

" Spacebar to toggle folds the cursor is over
nnoremap <Space> za
vnoremap <Space> za

" Set ,z to toggle all folds on/off
nnoremap <leader>f zi
vnoremap <leader>f zi

" Make zO recursively open whatever top level fold we're in, no matter where the
" cursor happens to be.
nnoremap zO zCzO

" "Focus" the current line.  Basically:
"
" 1. Close all folds.
" 2. Open just the folds containing the current line.
" 3. Move the line to a little bit (15 lines) above the center of the screen.
" 4. Pulse the cursor line.  My eyes are bad.
"
" This mapping wipes out the z mark, which I never use.
"
" I use :sus for the rare times I want to actually background Vim.
nnoremap <c-z> mzzMzvzz15<c-e>`z:Pulse<cr>

" Formatting options:
"   t: Auto-wrap text
"   c: Auto-wrap comments
"   r: Auto-insert comment leader after <enter>
"   o: Auto-insert comment leader after o or O
"   q: Allow formatting of comments with 'gq'
"   n: Recognize lists and indent
"   1: If possible, break lines before one-letter words, not after
set formatoptions+=c,r,q,n,1

" Filetype-specific ------------------------------------------------------- {{{
" C {{{
augroup ft_c
    au!
    au FileType c setlocal foldmethod=syntax
    au FileType c set omnifunc=ccomplete#Complete
    au Syntax {cpp,c,idl} runtime syntax/doxygen.vim
    " au Filetype c set spell
augroup END
" }}}
" C++ {{{
augroup ft_cpp
    au!
    au FileType c,cpp,h set foldmethod=syntax
    au Syntax {cpp,c,idl} runtime syntax/doxygen.vim
    " au Filetype cpp set spell
augroup END
" }}}
" Clojure {{{

augroup ft_clojure
    au!

    au BufNewFile,BufRead riemann.config set filetype=clojure
    au FileType clojure silent! call TurnOnClojureFolding()
    au FileType clojure compiler clojure
    au FileType clojure setlocal report=100000

    " Friendlier Paredit mappings.
    au FileType clojure noremap <buffer> () :<c-u>call PareditWrap("(", ")")<cr>
    au FileType clojure noremap <buffer> )( :<c-u>call PareditSplice()<cr>
    au FileType clojure noremap <buffer> (( :<c-u>call PareditMoveLeft()<cr>
    au FileType clojure noremap <buffer> )) :<c-u>call PareditMoveRight()<cr>
    au FileType clojure noremap <buffer> (j :<c-u>call PareditJoin()<cr>
    au FileType clojure noremap <buffer> (s :<c-u>call PareditSplit()<cr>
    au FileType clojure noremap <buffer> [ :<c-u>call PareditSmartJumpOpening(0)<cr>
    au FileType clojure noremap <buffer> ] :<c-u>call PareditSmartJumpClosing(0)<cr>
    " )))

    " Indent top-level form.
    au FileType clojure nmap <buffer> <localleader>= mz99[(v%='z
    " ])
augroup END

" }}}
" Clojurescript {{{

augroup ft_clojurescript
    au!

    au BufNewFile,BufRead *.cljs set filetype=clojurescript
    au FileType clojurescript call TurnOnClojureFolding()

    " Indent top-level form.
    au FileType clojurescript nmap <buffer> <localleader>= v((((((((((((=%
augroup END

" }}}
" Confluence {{{

augroup ft_c
    au!

    au BufRead,BufNewFile *.confluencewiki setlocal filetype=confluencewiki

    " Wiki pages should be soft-wrapped.
    au FileType confluencewiki setlocal wrap linebreak nolist
augroup END

" }}}
" Cram {{{

let cram_fold=1

augroup ft_cram
    au!

    au BufNewFile,BufRead *.t set filetype=cram
    au Syntax cram setlocal foldlevel=1
    au FileType cram nnoremap <buffer> <localleader>ee :e<cr>
augroup END

" }}}
" CSS and LessCSS {{{

augroup ft_css
    au!

    au BufNewFile,BufRead *.less setlocal filetype=less

    au Filetype less,css setlocal foldmethod=marker
    au Filetype less,css setlocal foldmarker={,}
    au Filetype less,css setlocal omnifunc=csscomplete#CompleteCSS
    au Filetype less,css setlocal iskeyword+=-

    " Use <leader>S to sort properties.  Turns this:
    "
    "     p {
    "         width: 200px;
    "         height: 100px;
    "         background: red;
    "
    "         ...
    "     }
    "
    " into this:

    "     p {
    "         background: red;
    "         height: 100px;
    "         width: 200px;
    "
    "         ...
    "     }
    au BufNewFile,BufRead *.less,*.css nnoremap <buffer> <localleader>S ?{<CR>jV/\v^\s*\}?$<CR>k:sort<CR>:noh<CR>

    " Make {<cr> insert a pair of brackets in such a way that the cursor is correctly
    " positioned inside of them AND the following code doesn't get unfolded.
    au BufNewFile,BufRead *.less,*.css inoremap <buffer> {<cr> {}<left><cr><space><space><space><space>.<cr><esc>kA<bs>
augroup END

" }}}
" Django {{{

augroup ft_django
    au!

    au BufNewFile,BufRead urls.py           setlocal nowrap
    au BufNewFile,BufRead urls.py           normal! zR
    au BufNewFile,BufRead dashboard.py      normal! zR
    au BufNewFile,BufRead local_settings.py normal! zR

    au BufNewFile,BufRead admin.py     setlocal filetype=python.django
    au BufNewFile,BufRead urls.py      setlocal filetype=python.django
    au BufNewFile,BufRead models.py    setlocal filetype=python.django
    au BufNewFile,BufRead views.py     setlocal filetype=python.django
    au BufNewFile,BufRead settings.py  setlocal filetype=python.django
    au BufNewFile,BufRead settings.py  setlocal foldmethod=marker
    au BufNewFile,BufRead forms.py     setlocal filetype=python.django
    au BufNewFile,BufRead common_settings.py  setlocal filetype=python.django
    au BufNewFile,BufRead common_settings.py  setlocal foldmethod=marker
augroup END

" }}}
" HTML, Django, Jinja, Dram {{{

augroup ft_html
    au!

    au BufNewFile,BufRead *.hbs setlocal filetype=html
    au BufNewFile,BufRead *.html setlocal filetype=html
    au BufNewFile,BufRead *.dram setlocal filetype=html

    au FileType html,jinja,htmldjango setlocal foldmethod=manual

    " Use <localleader>f to fold the current tag.
    au FileType html,jinja,htmldjango nnoremap <buffer> <localleader>f Vatzf

    " Use <localleader>t to fold the current templatetag.
    au FileType html,jinja,htmldjango nmap <buffer> <localleader>t viikojozf

    " Indent tag
    au FileType html,jinja,htmldjango nnoremap <buffer> <localleader>= Vat=

    " Django tags
    au FileType jinja,htmldjango inoremap <buffer> <c-t> {%<space><space>%}<left><left><left>

    " Django variables
    au FileType jinja,htmldjango inoremap <buffer> <c-b> {{<space><space>}}<left><left><left>

    autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
    autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

augroup END

" }}}
" Haskell {{{
augroup ft_haskell
    " Haskell post write lint and check with ghcmod
    " $ `cabal install ghcmod` if missing and ensure
    " ~/.cabal/bin is in your $PATH.
    if !executable("ghcmod")
        autocmd BufWritePost *.hs GhcModCheckAndLintAsync
    endif
    autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc
augroup END
" }}}
" Java {{{

augroup ft_java
    au!

    au FileType java setlocal foldmethod=marker
    au FileType java setlocal foldmarker={,}
augroup END

" }}}
" Javascript {{{

augroup ft_javascript
    au!

    au FileType javascript setlocal foldmethod=marker
    au FileType javascript setlocal foldmarker={,}
    au FileType javascript call MakeSpacelessBufferIabbrev('clog', 'console.log();<left><left>')

    " Make {<cr> insert a pair of brackets in such a way that the cursor is correctly
    " positioned inside of them AND the following code doesn't get unfolded.
    au Filetype javascript inoremap <buffer> {<cr> {}<left><cr><space><space><space><space>.<cr><esc>kA<bs>

    autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
augroup END

" }}}
" Lisp {{{

augroup ft_lisp
    au!
    au FileType lisp call TurnOnLispFolding()
    " au FileType lisp nnoremap <buffer> <localleader>ee mz99[(va("ry:call Send_to_Tmux(@r)<cr>
    " au FileType lisp nnoremap <buffer> <localleader>ee mz99[(:call SlimvEvalExp()<cr>`z

    " Using a plugin for this
    " let g:lisp_rainbow = 1

    " Fix the eval mappings.
    au FileType lisp nnoremap <buffer> <localleader>ef :call SlimvEvalExp()<cr>
    au FileType lisp nnoremap <buffer> <localleader>ee :call SlimvEvalDefun()<cr>

    au FileType lisp nnoremap <buffer> <localleader>tt mz0l99[(vab"ry:call Send_to_Tmux(@r)<cr>`z
    au FileType lisp nnoremap <buffer> <localleader>tb mzggVG"ry:call Send_to_Tmux(@r)<cr>`z

    " Indent top-level form.
    au FileType lisp nmap <buffer> <localleader>= mz99[(v%='z
augroup END

" }}}
" Markdown {{{

augroup ft_markdown
    au!
    au Filetype markdown set spell

augroup END

" }}}
" Mercurial {{{

augroup ft_mercurial
    au!

    au BufNewFile,BufRead *hg-editor-*.txt setlocal filetype=hgcommit
augroup END

" }}}
" Nginx {{{

augroup ft_nginx
    au!

    au BufRead,BufNewFile /etc/nginx/conf/*                      set ft=nginx
    au BufRead,BufNewFile /etc/nginx/sites-available/*           set ft=nginx
    au BufRead,BufNewFile /usr/local/etc/nginx/sites-available/* set ft=nginx
    au BufRead,BufNewFile vhost.nginx                            set ft=nginx

    au FileType nginx setlocal foldmethod=marker foldmarker={,}
augroup END

" }}}
" Python {{{

augroup ft_python
    au!

    au FileType python setlocal define=^\s*\\(def\\\\|class\\)
    au FileType man nnoremap <buffer> <cr> :q<cr>

    " au Filetype python set spell

    let python_highlight_all = 1

    "au FileType python if exists("python_space_error_highlight") | unlet python_space_error_highlight | endif
    autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
augroup END

" }}}
" QuickFix {{{

augroup ft_quickfix
    au!
    au Filetype qf setlocal colorcolumn=0 nolist nocursorline nowrap tw=0
augroup END

" }}}
" ReStructuredText {{{

augroup ft_rest
    au!

    au Filetype rst nnoremap <buffer> <localleader>1 yypVr=:redraw<cr>
    au Filetype rst nnoremap <buffer> <localleader>2 yypVr-:redraw<cr>
    au Filetype rst nnoremap <buffer> <localleader>3 yypVr~:redraw<cr>
    au Filetype rst nnoremap <buffer> <localleader>4 yypVr`:redraw<cr>
augroup END

" }}}
" Ruby {{{

augroup ft_ruby
    au!
    au Filetype ruby setlocal foldmethod=syntax
    autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
augroup END

" }}}
" Scala {{{

augroup ft_scala
    au!
    au Filetype scala setlocal foldmethod=indent
augroup END

" }}}
" TeX {{{

augroup ft_tex
    au!
    au Filetype tex set spell
augroup END

" }}}
" Vagrant {{{

augroup ft_vagrant
    au!
    au BufRead,BufNewFile Vagrantfile set ft=ruby
augroup END

" }}}
" Verilog {{{
augroup ft_verilog
    au!
    au FileType v,verilog set shiftwidth=3
augroup END
" }}}
" Vim {{{
augroup ft_vim
    au!

    au FileType vim setlocal foldmethod=marker
    au FileType help setlocal textwidth=78
    au BufWinEnter *.txt if &ft == 'help' | wincmd L | endif
augroup END
" }}}
" }}}


"""""""""""""""""PLUGIN""""""""""""""
Plugin 'tmhedberg/SimpylFold'

Plugin 'scrooloose/nerdtree'

" Plugin 'mileszs/ack.vim'

Plugin 'vim-scripts/indentpython.vim'

Plugin 'Valloric/YouCompleteMe'

Plugin 'scrooloose/syntastic'

Plugin 'nvie/vim-flake8'

Plugin 'altercation/vim-colors-solarized'

Plugin 'scrooloose/nerdcommenter'

Plugin 'jistr/vim-nerdtree-tabs'

Plugin 'kien/ctrlp.vim'

Plugin 'tpope/vim-fugitive'

Plugin 'airblade/vim-gitgutter'

Plugin 'vim-airline/vim-airline'

Plugin 'vim-airline/vim-airline-themes'

Plugin 'easymotion/vim-easymotion'

Plugin 'haya14busa/incsearch.vim'

Plugin 'haya14busa/incsearch-easymotion.vim'

Plugin 'klen/python-mode'

Plugin 'majutsushi/tagbar'

Plugin 'vim-scripts/taglist.vim'

Plugin 'vim-scripts/YankRing.vim'

Plugin 'flazz/vim-colorschemes'

Plugin 'xolox/vim-misc'

Plugin 'vim-scripts/csv.vim'
" Pending tasks list
Plugin 'fisadev/FixedTaskList.vim'

" Ack code search (requires ack installed in the system)
Plugin 'mileszs/ack.vim'
if has('python')
    " YAPF formatter for Python
    Plugin 'pignacio/vim-yapf-format'
endif
" Search results counter
Plugin 'vim-scripts/IndexedSearch'
" Plugin settings --------------------------------------------------------- {{{

Plugin 'christoomey/vim-tmux-navigator'

" Airline {{{
let g:airline_theme = 'dark'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
" }}}

" Ctrl-P {{{

let g:ctrlp_dont_split = 'NERD_tree_2'
let g:ctrlp_jump_to_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_match_window_reversed = 1
let g:ctrlp_split_window = 0
let g:ctrlp_max_height = 20
let g:ctrlp_extensions = ['tag']

let g:ctrlp_map = '<leader>.'
nnoremap <leader>. :CtrlPTag<cr>

let g:ctrlp_prompt_mappings = {
            \ 'PrtSelectMove("j")':   ['<c-j>', '<down>', '<s-tab>'],
            \ 'PrtSelectMove("k")':   ['<c-k>', '<up>', '<tab>'],
            \ 'PrtHistory(-1)':       ['<c-n>'],
            \ 'PrtHistory(1)':        ['<c-p>'],
            \ 'ToggleFocus()':        ['<c-tab>'],
            \ }

let ctrlp_filter_greps = "".
            \ "egrep -iv '\\.(" .
            \ "jar|class|swp|swo|log|so|o|pyc|jpe?g|png|gif|mo|po" .
            \ ")$' | " .
            \ "egrep -v '^(\\./)?(" .
            \ "deploy/|.git/|.hg/|.svn/|docs/build/|build/" .
            \ ")'"

let my_ctrlp_user_command = "" .
            \ "find %s -maxdepth 15 '(' -type f -or -type l ')' -not -path '*/\\.*/*' | " .
            \ ctrlp_filter_greps

let my_ctrlp_git_command = "" .
            \ "cd %s && git ls-files --exclude-standard -co | " .
            \ ctrlp_filter_greps

let my_ctrlp_ffind_command = "ffind --semi-restricted --dir %s --type e -B -f"

let g:ctrlp_user_command = ['.git/', my_ctrlp_git_command, my_ctrlp_user_command]
"Stop stupid window from popping up

" }}}

" EasyMotion {{{
" Move to line
map <Leader><Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader><Leader>L <Plug>(easymotion-overwin-line)

" You can use other keymappings like <C-l> instead of <CR> if you want to
" use these mappings as default search and somtimes want to move cursor with
" EasyMotion.
function! s:incsearch_config(...) abort
    return incsearch#util#deepextend(deepcopy({
                \   'modules': [incsearch#config#easymotion#module({'overwin': 1})],
                \   'keymap': {
                \     "\<CR>": '<Over>(easymotion)'
                \   },
                \   'is_expr': 0
                \ }), get(a:, 1, {}))
endfunction

noremap <silent><expr> <Leader><Leader>/  incsearch#go(<SID>incsearch_config())
noremap <silent><expr> <Leader><Leader>?  incsearch#go(<SID>incsearch_config({'command': '?'}))
noremap <silent><expr> <Leader><Leader>g/ incsearch#go(<SID>incsearch_config({'is_stay': 1}))
" }}}

" NERDTree {{{
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize = 50
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
nnoremap <silent> <F1> :NERDTreeFind<CR>
noremap <F2> :NERDTreeToggle<CR>
" }}}

" Fugitive {{{

nnoremap <leader>gd :Gdiff<cr>
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gw :Gwrite<cr>
nnoremap <leader>ga :Gadd<cr>
nnoremap <leader>gb :Gblame<cr>
nnoremap <leader>gl :Glog<cr>
nnoremap <leader>gco :Gcheckout<cr>
nnoremap <leader>gci :Gcommit<cr>
nnoremap <leader>gm :Gmove<cr>
nnoremap <leader>gr :Gremove<cr>
nnoremap <leader>gl :Shell git gl -18<cr>:wincmd \|<cr>

augroup ft_fugitive
    au!

    au BufNewFile,BufRead .git/index setlocal nolist
augroup END

" "Hub"
nnoremap <leader>H :Gbrowse<cr>
vnoremap <leader>H :Gbrowse<cr>

" }}}

" GitGutter {{{
let g:gitgutter_max_signs = 500   "default value

" }}}

" incsearch {{{
set hlsearch
let g:incsearch#auto_nohlsearch = 1
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)
" }}}

" Python-Mode {{{

let g:pymode_doc_key = 'M'
let g:pydoc = 'pydoc'
let g:pymode_run = 0

let g:pymode_rope = 0
let g:pymode_rope_global_prefix = "<localleader>R"
let g:pymode_rope_local_prefix = "<localleader>r"
let g:pymode_rope_autoimport_modules = ["os", "shutil", "datetime"]

let g:pymode_rope_complete_on_dot = 0

let g:pymode_lint_ignore = "E501,E265"

" }}}

" Syntastic {{{

let g:syntastic_enable_signs = 1
let g:syntastic_enable_balloons = 1
let g:syntastic_enable_highlighting = 1
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_check_on_open = 1
let g:syntastic_java_checker = 'javac'
let g:syntastic_mode_map = {
            \ "mode": "active",
            \ "active_filetypes": [],
            \ "passive_filetypes": ['java', 'html', 'rst', 'scala']
            \ }
let g:syntastic_stl_format = '[%E{%e Errors}%B{, }%W{%w Warnings}]'

let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_auto_refresh_includes = 1
let g:syntastic_cpp_compiler_options = ' -std=c++11'
let g:syntastic_cpp_remove_include_errors = 1

nnoremap <leader>C :SyntasticCheck<cr>

" }}}

" Tabular {{{
if exists(":Tabularize")
    nmap <leader>t= :Tabularize /=<CR>
    vmap <leader>t= :Tabularize /=<CR>
    nmap <leader>t: :Tabularize /:\zs<CR>
    vmap <leader>t: :Tabularize /:\zs<CR>
    nmap <Leader>t& :Tabularize /&<CR>
    vmap <Leader>t& :Tabularize /&<CR>
    nmap <Leader>t, :Tabularize /,<CR>
    vmap <Leader>t, :Tabularize /,<CR>
    nmap <Leader>t<Bar> :Tabularize /<Bar><CR>
    vmap <Leader>t<Bar> :Tabularize /<Bar><CR>
endif

function! s:align()
    let p = '^\s*|\s.*\s|\s*$'
    if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
        let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
        let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
        Tabularize/|/l1
        normal! 0
        call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
    endif
endfunction
" }}}

" Tagbar {{{
inoremap <F3> :TagbarToggle<CR>
nnoremap <F3> :TagbarToggle<CR>
vnoremap <F3> :TagbarToggle<CR>
" }}}

" Taglist {{{
inoremap <F4> :TlistToggle<CR>
nnoremap <F4> :TlistToggle<CR>
vnoremap <F4> :TlistToggle<CR>

" Taglist settings
let Tlist_Process_File_Always = 1
let Tlist_Auto_Highlight_Tag = 1
let Tlist_Auto_Update = 1
let Tlist_Enable_Fold_Column = 1
let Tlist_Highlight_Tag_On_BufEnter = 1
let Tlist_Max_Tag_Length = 35
let Tlist_Use_Right_Window = 1
let Tlist_Inc_Winwidth = 0
let Tlist_WinWidth = 40
" }}}

" YankRing {{{

nnoremap <silent> <F11> :YRShow<CR>
function! YRRunAfterMaps()
    " Make Y yank to end of line.
    nnoremap Y :<C-U>YRYankCount 'y$'<CR>

    " Fix L and H in operator-pending mode, so yH and such works.
    omap <expr> L YRMapsExpression("", "$")
    omap <expr> H YRMapsExpression("", "^")

    " Don't clobber the yank register when pasting over text in visual mode.
    vnoremap p :<c-u>YRPaste 'p', 'v'<cr>gv:YRYankRange 'v'<cr>
endfunction

" }}}

" youcompleteme {{{
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
" }}}

" SimplyFord {{{
let g:SimplyFold_docstring_preview=1
"" In order for SimpylFold to be properly loaded in certain cases.
autocmd BufWinEnter *.py setlocal foldexpr=SimpylFold(v:lnum) foldmethod=expr
autocmd BufWinLeave *.py setlocal foldexpr< foldmethod<
" }}}



" ACK {{{
map <Leader>a :Ack<CR>
" }}}

" vim-session {{{
" Do not save help file
set sessionoptions-=help

" Do not save hidden and unloaded buffers
"" set sessionoptions-=buffers

" Do not persist options and mappings because it can corrupt sessions
set sessionoptions-=options

let g:session_directory = '~/.vim/dirs/sessions/'
let g:session_default_name = 'vimsession'
let g:session_autosave = 'yes'
let g:session_command_aliases = 1

" }}}

" vim-color-solarized {{{
call togglebg#map("<F12>")
let g:solarized_termcolors=256
" let g:solarized_degrade = 1
let g:solarized_bold = 1
let g:solarized_underline = 1
let g:solarized_italic = 1
" let g:solarized_contrast = 'high'
" let g:solarized_visibility = 'high'
" }}}


" Environments (GUI / Console) {{{
if has('gui_running')
    set background=dark
    colorscheme codeschool
    " GUI Vim
    set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 10

    " Remove all the UI cruft
    set go-=T
    set go-=l
    set go-=L
    set go-=r
    set go-=R
    set go-=R
    set go-=m

    hi SpellBad   guisp=red    gui=undercurl guifg=NONE guibg=NONE ctermfg=red

    " Different cursors for different modes.
    set guicursor=n-c:block-Cursor-blinkon0
    set guicursor+=v:block-vCursor-blinkon0
    set guicursor+=i-ci:ver20-iCursor
else
    set background=dark
    colorscheme codeschool
    " Console Vim
    " Mouse support
    set mouse=
    " set mousehide               " Hide the mouse cursor while typing

    " Transparent console vim background
    hi Normal ctermbg=none
endif
" }}}

" ToggleZoomSplit {{{
" Zoom / Restore window.
function! s:ZoomToggle() abort
    if exists('t:zoomed') && t:zoomed
        execute t:zoom_winrestcmd
        let t:zoomed = 0
    else
        let t:zoom_winrestcmd = winrestcmd()
        resize
        vertical resize
        let t:zoomed = 1
    endif
endfunction
command! ZoomToggle call s:ZoomToggle()
nnoremap <silent> <C-x> :ZoomToggle<CR>
"  }}}

" RelativeNumber & AbsoluteNumber {{{
" Toggle Relative and Number
" Relative or absolute number lines
function! NumberToggle()
    if(&nu == 1)
        set nu!
        set rnu
    else
        set nornu
        set nu
    endif
endfunction

function! NumberOn()
    set rnu!
    set nu
endfunction

function! RelativeNumberOn()
    set nu!
    set rnu
endfunction

nnoremap <silentL <C-n> :call NumberToggle()<CR>

augroup linenumbers
    autocmd!
    autocmd BufEnter *    :set relativenumber
    autocmd BufLeave *    :set number norelativenumber
    autocmd WinEnter *    :set relativenumber
    autocmd WinLeave *    :set number norelativenumber
    autocmd InsertEnter * :set number norelativenumber
    autocmd InsertLeave * :set relativenumber
    autocmd FocusLost *   :set number norelativenumber
    autocmd FocusGained * :set relativenumber
augroup END

" }}}

" NertW Explore {{{
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_preview=1           " open previews vertically
let g:netrw_winsize = 25
"augroup ProjectDrawer
"autocmd!
"autocmd VimEnter * :Vexplore
"augroup END
function! ToggleVExplorer()
    if exists("t:expl_buf_num")
        let expl_win_num = bufwinnr(t:expl_buf_num)
        if expl_win_num != -1
            let cur_win_nr = winnr()
            exec expl_win_num . 'wincmd w'
            close
            exec cur_win_nr . 'wincmd w'
            unlet t:expl_buf_num
        else
            unlet t:expl_buf_num
        endif
    else
        exec '1wincmd w'
        Vexplore
        let t:expl_buf_num = bufnr("%")
    endif
endfunction

"noremap <silent> <F2> :call ToggleVExplorer()<CR>
"  }}}

" yank to clipboard
if has("clipboard")
    set clipboard=unnamed " copy to the system clipboard

    if has("unnamedplus") " X11 support
        set clipboard+=unnamedplus
    endif
endif


"====[ Make the 81st column stand out ]====================

" OR ELSE just the 81st column of wide lines...
highlight ColorColumn ctermbg=red
call matchadd('ColorColumn', '\%81v', 100)


" Mappings to access buffers (don't use "\p" because a
" " delay before pressing "p" would accidentally paste).
" " \l       : list buffers
" " \b \f \g : go back/forward/last-used
" " \1 \2 \3 : go to buffer 1/2/3 etc
nnoremap <Leader>l :ls<CR>
" nnoremap <Leader>b :bp<CR>
" nnoremap <Leader>f :bn<CR>
nnoremap <Leader>z :bp<CR>
nnoremap <Leader>m :bn<CR>
nnoremap <Leader>g :e#<CR>
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>
nnoremap <Space><Space>  :ls<CR>:b 

let c = 1
while c <= 99
    execute "nnoremap " . c . "<Space> :" . c . "b\<CR>"
    let c += 1
endwhile


" Tasklist ------------------------------

" show pending tasks list
map <F4> :TaskList<CR>


" CtrlP ------------------------------

" file finder mapping
let g:ctrlp_map = ',e'
" tags (symbols) in current file finder mapping
nmap ,g :CtrlPBufTag<CR>
" tags (symbols) in all files finder mapping
nmap ,G :CtrlPBufTagAll<CR>
" general code finder in all files mapping
nmap ,f :CtrlPLine<CR>
" recent files finder mapping
nmap ,m :CtrlPMRUFiles<CR>
" commands finder mapping
nmap ,c :CtrlPCmdPalette<CR>
" to be able to call CtrlP with default search text
function! CtrlPWithSearchText(search_text, ctrlp_command_end)
    execute ':CtrlP' . a:ctrlp_command_end
    call feedkeys(a:search_text)
endfunction
" same as previous mappings, but calling with current word as default text
nmap ,wg :call CtrlPWithSearchText(expand('<cword>'), 'BufTag')<CR>
nmap ,wG :call CtrlPWithSearchText(expand('<cword>'), 'BufTagAll')<CR>
nmap ,wf :call CtrlPWithSearchText(expand('<cword>'), 'Line')<CR>
nmap ,we :call CtrlPWithSearchText(expand('<cword>'), '')<CR>
nmap ,pe :call CtrlPWithSearchText(expand('<cfile>'), '')<CR>
nmap ,wm :call CtrlPWithSearchText(expand('<cword>'), 'MRUFiles')<CR>
nmap ,wc :call CtrlPWithSearchText(expand('<cword>'), 'CmdPalette')<CR>
" don't change working directory
let g:ctrlp_working_path_mode = 0
" ignore these files and folders on file finder
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.git|\.hg|\.svn|node_modules)$',
  \ 'file': '\.pyc$\|\.pyo$',
  \ }
"
"
" ------------------------------
source ~/.vim/vimrc.local
" for JSON --> sudo cpan JSON::XS
map <leader>jt  <Esc>:%!json_xs -f json -t json-pretty<CR>
" for SQL --> pip install sqlparse
map <leader>pt  :%!sqlformat --reindent --keywords upper --identifiers lower -<CR>
nnoremap <F9> O##TODO:REMOVE DEBUG<CR>import pdb; pdb.set_trace()<Esc><CR>

let g:yapf_format_yapf_location = '/usr/local/bin/yapf'
