
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/devel
export VIRTUAL_ENV=$HOME/.virtualenvs/trytond
source $HOME/.local/bin/virtualenvwrapper.sh
# alias tmux='tmux -2'
export TERM=screen-256color
